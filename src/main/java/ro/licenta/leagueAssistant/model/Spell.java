package ro.licenta.leagueAssistant.model;

public class Spell {
	String key;
	String description;
	String name;
	int spellSlot;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSpellSlot() {
		return spellSlot;
	}
	public void setSpellSlot(int spellSlot) {
		this.spellSlot = spellSlot;
	}
	
	
}
