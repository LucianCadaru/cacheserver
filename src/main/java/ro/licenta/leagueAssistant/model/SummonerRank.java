package ro.licenta.leagueAssistant.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SummonerRank {

	private int summonerId;
	private List<Rank> ranks;

	public Integer getSummonerId() {
		return summonerId;
	}

	public void setSummonerId(int summonerId) {
		this.summonerId = summonerId;
	}

	public List<Rank> getRanks() {
		return ranks;
	}

	public void setRanks(List<Rank> ranks) {
		this.ranks = ranks;
	}

	public Rank getHighestRank() {
		Map<String, Integer> tierRanking = new HashMap<>();
		tierRanking.put("BRONZE", 1);
		tierRanking.put("SILVER", 2);
		tierRanking.put("GOLD", 3);
		tierRanking.put("PLATINUM", 4);
		tierRanking.put("DIAMOND", 5);
		tierRanking.put("MASTER", 6);
		tierRanking.put("CHALLENGER", 7);

		Map<String, Integer> rankRanking = new HashMap<>();
		rankRanking.put("V", 1);
		rankRanking.put("IV", 2);
		rankRanking.put("III", 3);
		rankRanking.put("II", 2);
		rankRanking.put("I", 1);


		if (ranks.size() >= 2) {
			Rank max = ranks.get(0);
			for (int i = 1; i < ranks.size(); i++) {
				if (tierRanking.get(max.getTier()) < tierRanking.get(ranks.get(i).getTier())) {
					max = ranks.get(i);
				} else if (tierRanking.get(max.getTier()) == tierRanking.get(ranks.get(i).getTier())) {
					if (rankRanking.get(max.getRank()) < rankRanking.get(ranks.get(i).getRank())) {
						max = ranks.get(i);
					}
				}
			}

			return max;
		} else if (ranks.size() == 1) {
			return ranks.get(0);
		} else
			return null;
	}

}
