package ro.licenta.leagueAssistant.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Summoner.getSummonerByNameAndRegion", query = "SELECT s FROM Summoner s WHERE s.name=:name AND s.key.region=:region")
public class Summoner {

	@EmbeddedId
	private PrimaryKey key;
	private String name;
	private int summonerLevel;
	private long profileIconId;
	private long accountId;
	private long revisionDate;

	public PrimaryKey getKey() {
		return key;
	}

	public void setKey(PrimaryKey key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public int getSummonerLevel() {
		return summonerLevel;
	}

	public void setSummonerLevel(int summonerLevel) {
		this.summonerLevel = summonerLevel;
	}

	public long getProfileIconId() {
		return profileIconId;
	}

	public void setProfileIconId(long profileIconId) {
		this.profileIconId = profileIconId;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public long getRevisionDate() {
		return revisionDate;
	}

	public void setRevisionDate(long revisionDate) {
		this.revisionDate = revisionDate;
	}

}
