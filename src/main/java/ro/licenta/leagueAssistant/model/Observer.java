package ro.licenta.leagueAssistant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Observer {

    @SerializedName("encryptionKey")
    @Expose
    private String encryptionKey;

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

}
