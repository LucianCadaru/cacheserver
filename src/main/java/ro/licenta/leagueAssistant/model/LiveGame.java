package ro.licenta.leagueAssistant.model;

import java.util.List;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LiveGame {
	@SerializedName("gameId")
    @Expose
    private long gameId;
    @SerializedName("gameStartTime")
    @Expose
    private long gameStartTime;
    @SerializedName("platformId")
    @Expose
    private String platformId;
    @SerializedName("gameMode")
    @Expose
    private String gameMode;
    @SerializedName("mapId")
    @Expose
    private long mapId;
    @SerializedName("gameType")
    @Expose
    private String gameType;
    @SerializedName("gameQueueConfigId")
    @Expose
    private long gameQueueConfigId;
    @SerializedName("observers")
    @Expose
    private Observer observer;
    @SerializedName("participants")
    @Expose
    private List<LiveGameParticipant> participants = null;
    @SerializedName("gameLength")
    @Expose
    private long gameLength;
    @SerializedName("bannedChampions")
    @Expose
    private List<BannedChampion> bannedChampions = null;

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Long getGameStartTime() {
        return gameStartTime;
    }

    public void setGameStartTime(Long gameStartTime) {
        this.gameStartTime = gameStartTime;
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public String getGameMode() {
        return gameMode;
    }

    public void setGameMode(String gameMode) {
        this.gameMode = gameMode;
    }

    public long getMapId() {
        return mapId;
    }

    public void setMapId(long mapId) {
        this.mapId = mapId;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public Long getGameQueueConfigId() {
        return gameQueueConfigId;
    }

    public void setGameQueueConfigId(Long gameQueueConfigId) {
        this.gameQueueConfigId = gameQueueConfigId;
    }

    public Observer getObservers() {
        return observer;
    }

    public void setObservers(Observer observer) {
        this.observer = observer;
    }

    public List<LiveGameParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<LiveGameParticipant> participants) {
        this.participants = participants;
    }

    public long getGameLength() {
        return gameLength;
    }

    public void setGameLength(long gameLength) {
        this.gameLength = gameLength;
    }

    public List<BannedChampion> getBannedChampions() {
        return bannedChampions;
    }

    public void setBannedChampions(List<BannedChampion> bannedChampions) {
        this.bannedChampions = bannedChampions;
    }

}
