package ro.licenta.leagueAssistant.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LiveGameParticipant {

    @SerializedName("profileIconId")
    @Expose
    private long profileIconId;
    @SerializedName("championId")
    @Expose
    private long championId;
    @SerializedName("summonerName")
    @Expose
    private String summonerName;
    @SerializedName("runes")
    @Expose
    private List<Rune> runes = null;
    @SerializedName("bot")
    @Expose
    private Boolean bot;
    @SerializedName("masteries")
    @Expose
    private List<Mastery> masteries = null;
    @SerializedName("spell2Id")
    @Expose
    private long spell2Id;
    @SerializedName("teamId")
    @Expose
    private long teamId;
    @SerializedName("spell1Id")
    @Expose
    private long spell1Id;
    @SerializedName("summonerId")
    @Expose
    private long summonerId;

    public long getProfileIconId() {
        return profileIconId;
    }

    public void setProfileIconId(long profileIconId) {
        this.profileIconId = profileIconId;
    }

    public long getChampionId() {
        return championId;
    }

    public void setChampionId(long championId) {
        this.championId = championId;
    }

    public String getSummonerName() {
        return summonerName;
    }

    public void setSummonerName(String summonerName) {
        this.summonerName = summonerName;
    }

    public List<Rune> getRunes() {
        return runes;
    }

    public void setRunes(List<Rune> runes) {
        this.runes = runes;
    }

    public Boolean getBot() {
        return bot;
    }

    public void setBot(Boolean bot) {
        this.bot = bot;
    }

    public List<Mastery> getMasteries() {
        return masteries;
    }

    public void setMasteries(List<Mastery> masteries) {
        this.masteries = masteries;
    }

    public long getSpell2Id() {
        return spell2Id;
    }

    public void setSpell2Id(long spell2Id) {
        this.spell2Id = spell2Id;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public long getSpell1Id() {
        return spell1Id;
    }

    public void setSpell1Id(long spell1Id) {
        this.spell1Id = spell1Id;
    }

    public long getSummonerId() {
        return summonerId;
    }

    public void setSummonerId(long summonerId) {
        this.summonerId = summonerId;
    }

}
