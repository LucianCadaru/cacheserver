package ro.licenta.leagueAssistant.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PrimaryKey implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "id", nullable = false)
	private int id;
	@Column(name = "region", nullable = false)
	private String region;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (!(other instanceof PrimaryKey))
			return false;
		PrimaryKey castOther = (PrimaryKey) other;
		return id == castOther.id && region.equals(castOther.region);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + Integer.valueOf(this.id).hashCode();
		hash = hash * prime + this.region.hashCode();
		return hash;
	}

}
