package ro.licenta.leagueAssistant.model;

import java.util.List;

public class Champion {
	private int id;
	private String name;
	private String key;
	private List<Spell> spells;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public List<Spell> getSpells() {
		return spells;
	}
	public void setSpells(List<Spell> spells) {
		this.spells = spells;
	}
	
	
}
