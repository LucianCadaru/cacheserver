package ro.licenta.leagueAssistant.model;

public class ChampionBuild {

	private int championId;
	private String itemsHash;
	private String skillsHash;

	public int getChampionId() {
		return championId;
	}

	public void setChampionId(int championId) {
		this.championId = championId;
	}

	public String getItemsHash() {
		return itemsHash;
	}

	public void setItemsHash(String itemsHash) {
		this.itemsHash = itemsHash;
	}

	public String getSkillsHash() {
		return skillsHash;
	}

	public void setSkillsHash(String skillsHash) {
		this.skillsHash = skillsHash;
	}

}
