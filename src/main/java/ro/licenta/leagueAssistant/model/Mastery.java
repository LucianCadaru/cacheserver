package ro.licenta.leagueAssistant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mastery {

    @SerializedName("masteryId")
    @Expose
    private long masteryId;
    @SerializedName("rank")
    @Expose
    private int rank;

    public long getMasteryId() {
        return masteryId;
    }

    public void setMasteryId(long masteryId) {
        this.masteryId = masteryId;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

}