package ro.licenta.leagueAssistant.app;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import ro.licenta.leagueAssistant.service.BuildsRestService;
import ro.licenta.leagueAssistant.service.ChampionRestService;
import ro.licenta.leagueAssistant.service.IconRestService;
import ro.licenta.leagueAssistant.service.LiveGameRestService;
import ro.licenta.leagueAssistant.service.StaticDataRestService;
import ro.licenta.leagueAssistant.service.SummonerRankRestService;
import ro.licenta.leagueAssistant.service.SummonerRestService;


public class App extends Application {
	
	private Set<Class<?>> classes = new HashSet<>();

	public App() {
		classes.add(SummonerRestService.class);
		classes.add(IconRestService.class);
		classes.add(LiveGameRestService.class);
		classes.add(ChampionRestService.class);
		classes.add(BuildsRestService.class);
		classes.add(SummonerRankRestService.class);
		classes.add(StaticDataRestService.class);
	}

	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}

}
