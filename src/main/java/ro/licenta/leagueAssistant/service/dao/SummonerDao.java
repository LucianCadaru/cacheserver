package ro.licenta.leagueAssistant.service.dao;

import ro.licenta.leagueAssistant.model.Summoner;

public interface SummonerDao {
	
	Summoner saveSummoner(Summoner summoner);
	void removeSummoner(Summoner summoner);
	Summoner getSummoner(long id);
	Summoner getSummoner(String summonerName,String region);
	
}
