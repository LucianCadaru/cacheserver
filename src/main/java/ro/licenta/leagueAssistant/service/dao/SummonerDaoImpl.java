package ro.licenta.leagueAssistant.service.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ro.licenta.leagueAssistant.model.Summoner;

public class SummonerDaoImpl implements SummonerDao {

	@Inject
	private EntityManager entityManager;

	@Override
	public Summoner saveSummoner(Summoner summoner) {
		entityManager.getTransaction().begin();
		Summoner summonerToReturn = entityManager.merge(summoner);
		entityManager.getTransaction().commit();
		return summonerToReturn;
	}

	@Override
	public void removeSummoner(Summoner summoner) {
		entityManager.getTransaction().begin();
		entityManager.remove(summoner);
		entityManager.getTransaction().commit();
	}

	@Override
	public Summoner getSummoner(long id) {
		return entityManager.find(Summoner.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Summoner getSummoner(String summonerName,String region) {
		Query q = entityManager.createNamedQuery("Summoner.getSummonerByNameAndRegion");
		q.setParameter("name", summonerName);
		q.setParameter("region", region);
		List<Summoner> list = q.getResultList();
		if (list.isEmpty())
			return null;
		else
			return list.get(0);

	}

}
