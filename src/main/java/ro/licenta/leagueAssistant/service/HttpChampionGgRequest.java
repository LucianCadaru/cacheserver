package ro.licenta.leagueAssistant.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class HttpChampionGgRequest {
	
	private String apiKey;
	
	public HttpChampionGgRequest(){
		this.apiKey="6e16f5d4a4cfae6182924b37371b68eb";
	}
	
	public HttpResponse<JsonNode> getChampionBuild(int championId){
		String getUrl = "http://api.champion.gg/v2/champions/" + championId + "?champData=hashes&api_key="+apiKey;
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(getUrl).asJson();
			return response;
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}

}
