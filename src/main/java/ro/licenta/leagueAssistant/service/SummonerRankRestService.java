package ro.licenta.leagueAssistant.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

import ro.licenta.leagueAssistant.model.Rank;
import ro.licenta.leagueAssistant.model.SummonerRank;

@Path("/rank")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SummonerRankRestService {

	@Inject
	private HttpRiotRequest riotRequest;
	
	
	private CacheService cacheService=CacheServiceInstance.getInstance();

	@GET
	public Rank getSummonerRank(@QueryParam(value = "region") String region, @QueryParam(value = "id") int summonerId) {
		if (cacheService.getSummonerRank(summonerId) == null) {
			HttpResponse<JsonNode> response = riotRequest.getSummonerRank(summonerId, region);
			if (response.getStatus() == 200) {
				SummonerRank summonerRank = new SummonerRank();
				summonerRank.setSummonerId(summonerId);
				List<Rank> ranks = new ArrayList<>();
				JsonParser jsonParser = new JsonParser();
				JsonArray jsonArray = jsonParser.parse(response.getBody().toString()).getAsJsonArray();
				if (jsonArray.isJsonNull()) {
					summonerRank.setRanks(null);
				} else {
					Rank rank;
					for (int i = 0; i < jsonArray.size(); i++) {
						rank = new Rank();
						rank.setRank(jsonArray.get(i).getAsJsonObject().get("rank").getAsString());
						rank.setTier(jsonArray.get(i).getAsJsonObject().get("tier").getAsString());
						ranks.add(rank);
					}
					summonerRank.setRanks(ranks);
				}
				cacheService.setSummonerRank(summonerRank);
				return cacheService.getSummonerRank(summonerId).getHighestRank();
			} else
				return null;

		} else
			return cacheService.getSummonerRank(summonerId).getHighestRank();
	}

}