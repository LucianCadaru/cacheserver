package ro.licenta.leagueAssistant.service;

public class CacheServiceInstance {
	private static CacheService cacheServiceInstance;

	private CacheServiceInstance() {
	}

	//@Produces
	public static CacheService getInstance() {
		if (cacheServiceInstance == null) {
			cacheServiceInstance = new CacheService();
			return cacheServiceInstance;
		} else
			return cacheServiceInstance;
	}

}
