package ro.licenta.leagueAssistant.service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

import ro.licenta.leagueAssistant.model.PrimaryKey;
import ro.licenta.leagueAssistant.model.Summoner;
import ro.licenta.leagueAssistant.service.dao.SummonerDao;

@Path("/summoner")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SummonerRestService {

	@Inject
	private SummonerDao summonerDao;
	@Inject
	private HttpRiotRequest riotRequest;

	@GET
	public Summoner getSummoner(@QueryParam(value = "region") String region, @QueryParam(value = "name") String name) {
		if (summonerDao.getSummoner(name, region) == null) {
			HttpResponse<JsonNode> response = riotRequest.getSummoner(name, region);
			if (response.getStatus() == 200) {
				JsonParser jsonParser = new JsonParser();
				JsonElement jsonTree = jsonParser.parse(response.getBody().toString());
				JsonObject jsonObject = jsonTree.getAsJsonObject();
				Summoner summoner = new Summoner();
				summoner.setAccountId(jsonObject.get("accountId").getAsLong());
				summoner.setName(jsonObject.get("name").getAsString());
				summoner.setProfileIconId(jsonObject.get("profileIconId").getAsInt());
				summoner.setRevisionDate(jsonObject.get("revisionDate").getAsLong());
				summoner.setSummonerLevel(jsonObject.get("summonerLevel").getAsInt());
				PrimaryKey key = new PrimaryKey();
				key.setId(jsonObject.get("id").getAsInt());
				key.setRegion(region);
				summoner.setKey(key);
				summoner = summonerDao.saveSummoner(summoner);
				return summoner;
			} else
				return null;

		} else
			return summonerDao.getSummoner(name, region);
	}
	
	
	

}
