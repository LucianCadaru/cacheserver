package ro.licenta.leagueAssistant.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

import ro.licenta.leagueAssistant.model.Champion;
import ro.licenta.leagueAssistant.model.Spell;

@Path("/champion")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ChampionRestService {

	@Inject
	private HttpRiotRequest riotRequest;

	@GET
	public Champion getChampion(@QueryParam(value = "region") String region, @QueryParam(value = "id") int id) {
		
		HttpResponse<JsonNode> response = riotRequest.getChampion(id, region);
		if (response.getStatus() == 200) {
			Champion champion= new Champion();
			JsonParser jsonParser=new JsonParser();
			JsonElement jsonTree = jsonParser.parse(response.getBody().toString());
			JsonObject jsonObject = jsonTree.getAsJsonObject();
			JsonArray jsonArray = jsonObject.get("spells").getAsJsonArray();
			List<Spell> spells = new ArrayList<>();
			for(int i=0;i<jsonArray.size();i++){
				Spell spell = new Spell();
				JsonObject spellJson = jsonArray.get(i).getAsJsonObject();
				spell.setDescription(spellJson.get("description").getAsString());
				spell.setKey(spellJson.get("key").getAsString());
				spell.setName(spellJson.get("name").getAsString());
				spell.setSpellSlot(i);
				spells.add(spell);
			}
			champion.setName(jsonObject.get("name").getAsString());
			champion.setId(jsonObject.get("id").getAsInt());
			champion.setKey(jsonObject.get("key").getAsString());
			champion.setSpells(spells);
			return champion;
		} else
			return null;

	}

}
