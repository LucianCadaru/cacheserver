package ro.licenta.leagueAssistant.service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/icons")
@Consumes(MediaType.APPLICATION_JSON)
@Produces("image/png")
public class IconRestService {

	@Inject
	private HttpRiotRequest riotRequest;

	private CacheService cacheService=CacheServiceInstance.getInstance();
	
	@GET
	@Path("/summoner")
	public byte[] getSummonerIcon(@QueryParam(value = "id") int iconId, @QueryParam(value = "region") String region) {
		String version;
		if(cacheService.getLatestVersion()== null){
			version = riotRequest.getLatestVersion(region);
			cacheService.setLatestVersion(version);
		}
		else 
			version = cacheService.getLatestVersion();
		return riotRequest.getIcon(version, iconId);	
	}
	
}
