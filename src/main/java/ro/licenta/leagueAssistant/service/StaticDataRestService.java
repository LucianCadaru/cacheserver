package ro.licenta.leagueAssistant.service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/data")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StaticDataRestService {

	@Inject
	private HttpRiotRequest riotRequest;
	
	
	private CacheService cacheService=CacheServiceInstance.getInstance();

	@GET
	@Path("/runes/{region}")
	public String getRunesData(@PathParam("region") String region) {
		String version;
		if(cacheService.getLatestVersion()== null){
			version = riotRequest.getLatestVersion(region);
			cacheService.setLatestVersion(version);
		}
		else 
			version = cacheService.getLatestVersion();
		return riotRequest.getRunes(region,version);	
		
	}
	
	@GET
	@Path("/items/{region}")
	public String getItemsData(@PathParam("region") String region) {
		String version;
		if(cacheService.getLatestVersion()== null){
			version = riotRequest.getLatestVersion(region);
			cacheService.setLatestVersion(version);
		}
		else 
			version = cacheService.getLatestVersion();
		return riotRequest.getItems(region,version);	
		
	}
	
	@GET
	@Path("/masteries/{region}")
	public String getMasteriesData(@PathParam("region") String region) {
		String version;
		if(cacheService.getLatestVersion()== null){
			version = riotRequest.getLatestVersion(region);
			cacheService.setLatestVersion(version);
		}
		else 
			version = cacheService.getLatestVersion();
		return riotRequest.getMasteries(region,version);	
		
	}

}