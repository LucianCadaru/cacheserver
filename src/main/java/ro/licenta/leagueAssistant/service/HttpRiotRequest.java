package ro.licenta.leagueAssistant.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class HttpRiotRequest {

	private String apiKey;
	private Map<String, String> regionMap;

	public HttpRiotRequest() {
		apiKey = "RGAPI-5fed81d3-3e25-4820-9fac-2d539bdd9ba0";
		regionMap = new HashMap<>();
		regionMap.put("EUNE", "eun1");
		regionMap.put("EUW", "euw1");
		regionMap.put("RU", "ru");
		regionMap.put("KR", "kr");
		regionMap.put("BR", "br1");
		regionMap.put("OCE", "oc1");
		regionMap.put("JP", "jp1");
		regionMap.put("NA", "na1");
		regionMap.put("TR", "tr1");
		regionMap.put("LAN", "la1");
		regionMap.put("LAS", "la2");
	}

	public HttpResponse<JsonNode> getSummoner(String summonerName, String region) {
		String getUrl = buildUrl(region) + "/lol/summoner/v3/summoners/by-name/" + summonerName;
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(getUrl).header("X-Riot-Token", apiKey).asJson();
			return response;
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}

	public HttpResponse<JsonNode> getLiveGame(Long id, String region) {
		String getUrl = buildUrl(region) + "/lol/spectator/v3/active-games/by-summoner/" + id;
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(getUrl).header("X-Riot-Token", apiKey).asJson();
			return response;
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}

	public byte[] getIcon(String version, int iconId) {
		String getUrl = "http://ddragon.leagueoflegends.com/cdn/" + /*version*/"7.12.1" + "/img/profileicon/" + iconId + ".png";
		
		BufferedImage image;
		try {
			URL url = new URL(getUrl);
			image = ImageIO.read(url);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "png", baos);
			byte[] imageData = baos.toByteArray();
			return  imageData;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	public String getLatestVersion(String region) {
		String getUrl = buildUrl(region) + "/lol/static-data/v3/versions";
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(getUrl).header("X-Riot-Token", apiKey).asJson();
			return (String) response.getBody().getArray().get(0).toString();
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String buildUrl(String region) {
		return "https://" + regionMap.get(region) + ".api.riotgames.com";
	}

	public HttpResponse<JsonNode> getChampion(int id, String region) {
		String getUrl = buildUrl(/*region*/"NA") + "/lol/static-data/v3/champions/" + id
				+ "?locale=en_US&tags=allytips&tags=passive&tags=spells";
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(getUrl).header("X-Riot-Token", apiKey).asJson();
			return response;
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;

	}

	
	public HttpResponse<JsonNode> getSummonerRank(long summonerId, String region) {
		String getUrl = buildUrl(region) + "/lol/league/v3/positions/by-summoner/" + summonerId;
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(getUrl).header("X-Riot-Token", apiKey).asJson();
			return response;
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public String getRunes(String region,String version) {
		String getUrl = buildUrl(region) + "/lol/static-data/v3/runes?locale=en_US&tags=image";
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(getUrl).header("X-Riot-Token", apiKey).asJson();
			return response.getBody().toString();
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getMasteries(String region,String version) {
		String getUrl = buildUrl(region) + "/lol/static-data/v3/masteries";
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(getUrl).header("X-Riot-Token", apiKey).asJson();
			return response.getBody().toString();
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getItems(String region,String version) {
		String getUrl = buildUrl(region) + "/lol/static-data/v3/items?locale=en_US&tags=gold";
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(getUrl).header("X-Riot-Token", apiKey).asJson();
			return response.getBody().toString();
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}

}
