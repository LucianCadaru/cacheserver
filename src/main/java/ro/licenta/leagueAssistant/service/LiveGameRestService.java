package ro.licenta.leagueAssistant.service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

import ro.licenta.leagueAssistant.model.LiveGame;

@Path("/live")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LiveGameRestService {

	@Inject
	private HttpRiotRequest riotRequest;

	private CacheService cacheService=CacheServiceInstance.getInstance();

	@GET
	public LiveGame getLiveGame(@QueryParam("summonerId") long id, @QueryParam("region") String region) {
		if (cacheService.getLiveGame(id) == null) {
			HttpResponse<JsonNode> response = riotRequest.getLiveGame(id, region);
			if (response.getStatus() == 200) {
				Gson gson = new Gson();
				LiveGame liveGame = gson.fromJson(response.getBody().toString(), LiveGame.class);
				cacheService.setLiveGame(liveGame.getGameId(), liveGame);
				return liveGame;
			} else
				return null;
		} else
			return cacheService.getLiveGame(id);
	}
}
