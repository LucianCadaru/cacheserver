package ro.licenta.leagueAssistant.service;

import java.net.URL;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.xml.XmlConfiguration;

import ro.licenta.leagueAssistant.model.ChampionBuild;
import ro.licenta.leagueAssistant.model.LiveGame;
import ro.licenta.leagueAssistant.model.SummonerRank;

public class CacheService {

	private CacheManager cacheManager;
	private Cache<String, String> versionCache;
	private Cache<Long, LiveGame> liveGameCache;
	private Cache<Integer, ChampionBuild> buildsCache;
	private Cache<Integer, SummonerRank> summonerRankCache;

	public CacheService() {

		URL myUrl = getClass().getResource("/ehcache.xml");
		Configuration xmlConfig = new XmlConfiguration(myUrl);
		cacheManager = CacheManagerBuilder.newCacheManager(xmlConfig);
		cacheManager.init();
		
		versionCache = cacheManager.getCache("versionCache", String.class, String.class);
		liveGameCache = cacheManager.getCache("liveGameCache", Long.class, LiveGame.class);
		buildsCache = cacheManager.getCache("buildsCache", Integer.class, ChampionBuild.class);
		summonerRankCache = cacheManager.getCache("summonerRankCache", Integer.class, SummonerRank.class);

	}

	public String getLatestVersion() {
		return versionCache.get("version");
	}

	public void setLatestVersion(String version) {
		versionCache.put("version", version);
	}

	public LiveGame getLiveGame(long id) {
		return liveGameCache.get(id);
	}

	public void setLiveGame(long id, LiveGame liveGame) {
		liveGameCache.put(id, liveGame);
	}

	public ChampionBuild getBuild(int championId) {
		return buildsCache.get(championId);
	}

	public void setBuilds(int championId, ChampionBuild build) {
		buildsCache.put(championId, build);
	}

	public void setSummonerRank(SummonerRank summonerRank) {
		summonerRankCache.put(summonerRank.getSummonerId(), summonerRank);
	}

	public SummonerRank getSummonerRank(int summonerId) {
		return summonerRankCache.get(summonerId);
	}

	public SummonerRank getSummonerRank(SummonerRank summonerRank) {
		return summonerRankCache.get(summonerRank.getSummonerId());
	}

	public void close() {
		cacheManager.close();
	}

}
