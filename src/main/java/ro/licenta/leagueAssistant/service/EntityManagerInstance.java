package ro.licenta.leagueAssistant.service;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerInstance {

	private static EntityManager instance;

	private EntityManagerInstance() {
	}

	@Produces
	public static EntityManager getInstance() {
		if (instance == null) {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("league-pu");
			instance = emf.createEntityManager();
			return instance;
		} else
			return instance;
	}
}