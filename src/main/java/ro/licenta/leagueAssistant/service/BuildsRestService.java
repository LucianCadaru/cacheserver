package ro.licenta.leagueAssistant.service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

import ro.licenta.leagueAssistant.model.ChampionBuild;

@Path("/builds")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BuildsRestService {

	@Inject
	private HttpChampionGgRequest championGgRequest;

	private CacheService cacheService=CacheServiceInstance.getInstance();

	@GET
	@Path("/{championId}")
	public ChampionBuild getBuild(@PathParam("championId") int championId) {
		if (cacheService.getBuild(championId) == null) {
			HttpResponse<JsonNode> response = championGgRequest.getChampionBuild(championId);
			JsonParser jsonParser = new JsonParser();
			JsonArray jsonArray = jsonParser.parse(response.getBody().toString()).getAsJsonArray();
			JsonObject max = null;
			if (jsonArray.size() <= 1) {
				max = jsonArray.get(0).getAsJsonObject();
			} else {
				for (int i = 0; i < jsonArray.size() - 1; i++) {
					JsonObject jsonElement1 = jsonArray.get(i).getAsJsonObject();
					JsonObject jsonElement2 = jsonArray.get(i + 1).getAsJsonObject();
					if (jsonElement1.get("gamesPlayed").getAsInt() >= jsonElement2.get("gamesPlayed").getAsInt()) {
						max = jsonElement1;
					} else {
						max = jsonElement2;
					}
				}
			}

			ChampionBuild newBuild = new ChampionBuild();
			newBuild.setChampionId(max.get("championId").getAsInt());
			newBuild.setItemsHash(max.get("hashes").getAsJsonObject().get("finalitemshashfixed").getAsJsonObject()
					.get("highestWinrate").getAsJsonObject().get("hash").getAsString());
			newBuild.setSkillsHash(max.get("hashes").getAsJsonObject().get("skillorderhash").getAsJsonObject()
					.get("highestWinrate").getAsJsonObject().get("hash").getAsString());
			cacheService.setBuilds(newBuild.getChampionId(), newBuild);
			return newBuild;
		} else
			return cacheService.getBuild(championId);
	}
}
